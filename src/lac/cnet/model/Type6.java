package lac.cnet.model;

import java.io.Serializable;
import java.util.HashMap;

public class Type6 implements Serializable
{
    //private static final long serialVersionUID = 3154079683997509100L;
    private static final long serialVersionUID = 209L;
    
    public int     member1  = 0;
    public double  member2  = 0;
    public double  member3  = 0;
    public String  member4  = null;
    public float   member5  = 0;
    public int     member6  = 0;
    public double  member7  = 0;
    public double  member8  = 0;
    public String  member9  = null;
    public float   member10 = 0;
    public int     member11 = 0;
    public double  member12 = 0;
    public double  member13 = 0;
    public String  member14 = null;
    public float   member15 = 0;
    public int     member16 = 0;
    public double  member17 = 0;
    public double  member18 = 0;
    public String  member19 = null;
    public float   member20 = 0;
    public int     member21 = 0;
    public double  member22 = 0;
    public double  member23 = 0;
    public String  member24 = null;
    public float   member25 = 0;
    
    public int     areaId;
    
    public Type6()
    {
        super();
    }
    
    public Type6(int member1, double member2, double member3, String member4, float member5, int member6, double member7, double member8, String member9, float member10, int member11, double member12, double member13, String member14, float member15, int member16, double member17, double member18, String member19, float member20, int member21, double member22, double member23, String member24,
            float member25)
    {
        super();
        this.member1 = member1;
        this.member2 = member2;
        this.member3 = member3;
        this.member4 = member4;
        this.member5 = member5;
        this.member6 = member6;
        this.member7 = member7;
        this.member8 = member8;
        this.member9 = member9;
        this.member10 = member10;
        this.member11 = member11;
        this.member12 = member12;
        this.member13 = member13;
        this.member14 = member14;
        this.member15 = member15;
        this.member16 = member16;
        this.member17 = member17;
        this.member18 = member18;
        this.member19 = member19;
        this.member20 = member20;
        this.member21 = member21;
        this.member22 = member22;
        this.member23 = member23;
        this.member24 = member24;
        this.member25 = member25;
    }

    public int getMember1()
    {
        return member1;
    }
    public void setMember1(int member1)
    {
        this.member1 = member1;
    }
    public double getMember2()
    {
        return member2;
    }
    public void setMember2(double member2)
    {
        this.member2 = member2;
    }
    public double getMember3()
    {
        return member3;
    }
    public void setMember3(double member3)
    {
        this.member3 = member3;
    }
    public String getMember4()
    {
        return member4;
    }
    public void setMember4(String member4)
    {
        this.member4 = member4;
    }
    public float getMember5()
    {
        return member5;
    }
    public void setMember5(float member5)
    {
        this.member5 = member5;
    }
    public int getMember6()
    {
        return member6;
    }
    public void setMember6(int member6)
    {
        this.member6 = member6;
    }
    public double getMember7()
    {
        return member7;
    }
    public void setMember7(double member7)
    {
        this.member7 = member7;
    }
    public double getMember8()
    {
        return member8;
    }
    public void setMember8(double member8)
    {
        this.member8 = member8;
    }
    public String getMember9()
    {
        return member9;
    }
    public void setMember9(String member9)
    {
        this.member9 = member9;
    }
    public float getMember10()
    {
        return member10;
    }
    public void setMember10(float member10)
    {
        this.member10 = member10;
    }
    public int getMember11()
    {
        return member11;
    }
    public void setMember11(int member11)
    {
        this.member11 = member11;
    }
    public double getMember12()
    {
        return member12;
    }
    public void setMember12(double member12)
    {
        this.member12 = member12;
    }
    public double getMember13()
    {
        return member13;
    }
    public void setMember13(double member13)
    {
        this.member13 = member13;
    }
    public String getMember14()
    {
        return member14;
    }
    public void setMember14(String member14)
    {
        this.member14 = member14;
    }
    public float getMember15()
    {
        return member15;
    }
    public void setMember15(float member15)
    {
        this.member15 = member15;
    }
    public int getMember16()
    {
        return member16;
    }
    public void setMember16(int member16)
    {
        this.member16 = member16;
    }
    public double getMember17()
    {
        return member17;
    }
    public void setMember17(double member17)
    {
        this.member17 = member17;
    }
    public double getMember18()
    {
        return member18;
    }
    public void setMember18(double member18)
    {
        this.member18 = member18;
    }
    public String getMember19()
    {
        return member19;
    }
    public void setMember19(String member19)
    {
        this.member19 = member19;
    }
    public float getMember20()
    {
        return member20;
    }
    public void setMember20(float member20)
    {
        this.member20 = member20;
    }
    public int getMember21()
    {
        return member21;
    }
    public void setMember21(int member21)
    {
        this.member21 = member21;
    }
    public double getMember22()
    {
        return member22;
    }
    public void setMember22(double member22)
    {
        this.member22 = member22;
    }
    public double getMember23()
    {
        return member23;
    }
    public void setMember23(double member23)
    {
        this.member23 = member23;
    }
    public String getMember24()
    {
        return member24;
    }
    public void setMember24(String member24)
    {
        this.member24 = member24;
    }
    public float getMember25()
    {
        return member25;
    }
    public void setMember25(float member25)
    {
        this.member25 = member25;
    }
    
    public int getAreaId()
    {
        return areaId;
    }
    public void setAreaId(int area)
    {
        this.areaId = area;
    }
    
    @Override
    public String toString()
    {
        return "Type6 [member1=" + member1 + ", member2=" + member2 + ", member3=" + member3 + ", member4=" + member4 + ", member5=" + member5 + ", member6=" + member6 + ", member7=" + member7 + ", member8=" + member8 + ", member9=" + member9 + ", member10=" + member10 + ", member11=" + member11 + ", member12=" + member12 + ", member13=" + member13 + ", member14=" + member14 + ", member15="
                + member15 + ", member16=" + member16 + ", member17=" + member17 + ", member18=" + member18 + ", member19=" + member19 + ", member20=" + member20 + ", member21=" + member21 + ", member22=" + member22 + ", member23=" + member23 + ", member24=" + member24 + ", member25=" + member25 + "]";
    }
    
    public static HashMap<String,Object> getHashMapRepresentation(Type6 type6)
    {
        HashMap<String,Object> newInstance = new HashMap<String, Object>();
        
        newInstance.put("member1", type6.member1);
        newInstance.put("member2", type6.member2);
        newInstance.put("member3", type6.member3);
        newInstance.put("member4", type6.member4);
        newInstance.put("member5", type6.member5);
        newInstance.put("member6", type6.member6);
        newInstance.put("member7", type6.member7);
        newInstance.put("member8", type6.member8);
        newInstance.put("member9", type6.member9);
        newInstance.put("member10", type6.member10);
        newInstance.put("member11", type6.member11);
        newInstance.put("member12", type6.member12);
        newInstance.put("member13", type6.member13);
        newInstance.put("member14", type6.member14);
        newInstance.put("member15", type6.member15);
        newInstance.put("member16", type6.member16);
        newInstance.put("member17", type6.member17);
        newInstance.put("member18", type6.member18);
        newInstance.put("member19", type6.member19);
        newInstance.put("member20", type6.member20);
        newInstance.put("member21", type6.member21);
        newInstance.put("member22", type6.member22);
        newInstance.put("member23", type6.member23);
        newInstance.put("member24", type6.member24);
        newInstance.put("member25", type6.member25);

        return newInstance;
    }
    
    public static Type6 createSampleType6Event()
    {
        int     member1  = 1;
        double  member2  = 1;
        double  member3  = 1;
        String  member4  = "23/04/2015";
        float   member5  = 1;
        int     member6  = 1;
        double  member7  = 1;
        double  member8  = 1;
        String  member9  = "23/04/2015";
        float   member10 = 1;
        int     member11 = 1;
        double  member12 = 1;
        double  member13 = 1;
        String  member14 = "23/04/2015";
        float   member15 = 1;
        int     member16 = 1;
        double  member17 = 1;
        double  member18 = 1;
        String  member19 = "23/04/2015";
        float   member20 = 1;
        int     member21 = 1;
        double  member22 = 1;
        double  member23 = 1;
        String  member24 = "23/04/2015";
        float   member25 = 1;

        Type6 newInstance = new Type6(member1, member2, member3, member4, member5, member6, member7, member8, member9, member10, member11, member12, member13, member14, member15, member16, member17, member18, member19, member20, member21, member22, member23, member24, member25);
        
        return newInstance;
    }
    
    public static Type6 createSampleType6Event(int areaId)
    {
        Type6 newInstance = createSampleType6Event();
        
        newInstance.areaId = areaId;
        
        return newInstance;
    }
}
